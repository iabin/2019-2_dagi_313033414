import Snek from './Snek.js'
const MS_PER_FRAME = 100;

export default class Game {//Main thread
    constructor(height,width) {
        this.board = document.getElementById("board");//El tablero
        this.scoreboard = document.getElementById("score");//El score
        this.snake = new Snek(3, 5);//Nueva serpiente con esa posiciÃ³n
        this.score = 0;//Empieza en 0
        this.height =  height;
        this.width = width;
        this.hud = document.getElementById("HUD");//El hud de gameOVer
        this.retry_button = document.getElementById("retry");//El botÃ³n de jugar otra vez
        this.finished = false;//Revisa si el juego estÃ¡ acabado
        this.speed = this.snake.speed;//La velocidad de la serpiente, 
        //Se actualiza aquÃ­ y en cada ciclo se le pasa a la serpiente
        
    }

    //Prepara la entrada
    setup(){
        this.input();
    }
    
    //La funciÃ³n main del juego
    async start_game() {
        this.new_apple();
        setInterval(() => {//Ciclo que se ejecuta cada segundo Como uso this.snake uso la lambda
            if (this.finished){
                return;
            }
            this.update_and_render();
        }, MS_PER_FRAME);
    }

    update_score(){
        //Es llamado cada que come manzana, solo actualiza el score
        this.scoreboard.innerHTML = "Score: "+this.score;
    }

    move_and_update_eating(){
        this.score += 1;
        this.update_score();
        this.snake.next_move_eating();
        this.snake.body.forEach(element => {
            let casilla = document.getElementById("row:" + element.y + "_column:" + element.x);
            casilla.setAttribute("state", "snake")
        });
        this.new_apple();
    }

    move_and_update_no_eating(){
        let old = this.snake.next_move_no_eating();
            this.snake.body.forEach(element => {
                let casilla = document.getElementById("row:" + element.y + "_column:" + element.x);
                casilla.setAttribute("state", "snake")
            });
            let casilla = document.getElementById("row:" + old.y + "_column:" + old.x);
            casilla.setAttribute("state", "board");
    }

    update_and_render() {//TratÃ© de separarlos pero, para saber el estado del tablero necesito acceso al document
        this.snake.setSpeed(this.speed.x,this.speed.y);//Le pongo la direcciÃ³n dada por la entrada 
        let potencial_move = this.snake.potencial_next_move();//Reviso el potencial 
        //Siguiente movimiento para revisar si perdÃ­ y obtener la casillas
        let potencial_casilla = document.getElementById("row:" + potencial_move.y + "_column:" + potencial_move.x);
        //Si es null significa que no existe esa casilla, eso significa que no existe y se terminÃ³ el tablero
        if(potencial_casilla == null){
            this.gameOver();//LLamos el gameOver
            return;
        }
        //Obtengo el estado de esa casilla para saber si es manzana, tablero o serpiente
        let state = potencial_casilla.getAttribute("state");
        //Si es una manzana, Llamo al mÃ©todo que come aunmenta el tamaÃ±o y actualiza el board
        if (state == "apple") {
           this.move_and_update_eating();
           //Solo me muevo y renderizo
        } else if( (state == "board")){
            this.move_and_update_no_eating();
            //LLamo al game over
        } else if( (state == "snake")){
            this.gameOver();
            return;
        }
    }

    //Genera una nueva manzana en una poscisiÃ³n random
    new_apple(){
        let x = Math.floor(Math.random() * this.height);
        let y = Math.floor(Math.random() * this.width);
        //obtengo la casilla random 
        let potencial_casilla = document.getElementById("row:" + y + "_column:" + x);
        //Obtengo su esado
        let state = potencial_casilla.getAttribute("state");
        //Si tiene estado tablero, pongo la manzana ahÃ­
        if ( state === "board"){
            potencial_casilla.setAttribute("state","apple");
            return;
        }
        //Si no es tablero, es serpiernte y vuelvo a llamar a la funciÃ³n
        return this.new_apple();
    }


    gameOver(){
        //El hud de game over lo pongo visible
        this.hud.style.visibility = "visible";
        //La bandera de finished es true
        this.finished = true;
    }

    //Se carga al principio y carga los listeners
    input(){
        //Es el boton de game over, recarga la pÃ¡gina
        this.retry_button.addEventListener("click",() => {
            location.reload();
        });
        //Carga los eventos del teclado
        addEventListener("keydown", (event)=> {
            //Cada click modifica la direcciÃ³n en el juego, y se actualiza en la serpiente
            //En la funciÃ³n update an render
            if (event.keyCode == 37){//Left arrow
                this.speed = this.snake.newVector(-1,0);
            }else if(event.keyCode == 38){//Up arrow
                this.speed = this.snake.newVector(0,-1);
            }else if(event.keyCode == 39){ //Right arrow
                this.speed = this.snake.newVector(1,0);
            }else if (event.keyCode == 40){//Down arrow
                this.speed = this.snake.newVector(0,1);
            }
           
          },false);
    }




}

