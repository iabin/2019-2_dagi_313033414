export default class Snek{//dont step on snek
    constructor(x,y){
        this.size = 3;//El tamaño de inicio de la serpiente es 3
        this.speed = new Vector(0,1);//Este vector representa la velocidad y dirección de la serpiente
        //Y siempre camina hacia la derecha
        this.body = [new Vector(x-2,y),new Vector(x-1,y),new Vector(x,y)]//Es mi cuerpo, es una cola, por lo tanto
        //El elemento más viejo es el que se irá eliminando
        this.head = this.body[this.body.length - 1];//El último elemento de la cola es la cabeza
    }

    //Me muevo sin comer, me muevo en la dirección que tiene definida mi velocidad
    next_move_no_eating(){
        //Es la nueva cabeza de la serpiente, es equivalente a una linked list
        let new_head = new Vector(this.head.x+this.speed.x, this.head.y+this.speed.y);
        //Meto la nueva cabeza a la cola
        this.body.push(new_head);
        //Actualizo el apuntador
        this.head = new_head;
        //Hago pop de la cola al último elemento y así da la ilusión de moverse
        let old = this.body.shift();
        //Regreso ese vector, me servirá para desrenderizarlo
        return old;
    }

    //Es lo mismo solo que no saco ningún cuadro de la cola porque estoy comiendo,
    next_move_eating(){
        //La nueva cabeza
        let new_head = new Vector(this.head.x+this.speed.x, this.head.y+this.speed.y);
        //Le hago push, y como crezco uno no tengo que hacer push
        this.body.push(new_head);
        //actualizo el apuntador de la nueva cabeza
        this.head = new_head;
        //Aumento el apuntador de la cabeza
        this.size += 1;
    }
    newVector(x,y){
        return new Vector(x,y);
    }
    //Uso este método para saber el próximo cuadro y revisar si hay algo ahí
    potencial_next_move(){
        //Es el vector del pusible siguente movimiento
        let new_head = new Vector(this.head.x+this.speed.x, this.head.y+this.speed.y);
        return new_head;
    }
    setSpeed(x,y){
        let velo = this.speed;
        if (!(velo.x === x*-1 && velo.y === y*-1)){
            this.speed = new Vector(x,y);
        }
        return;
    }
}

//Modela vectores
class Vector{
 constructor(x,y){
     this.x = x;
     this.y = y;
 }
}