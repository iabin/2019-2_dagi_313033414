
import Game from './Game.js';

const height = 20;//Largo
const width = 20;//Alto

//Al terminar de cargar se ejecuta esto
window.addEventListener("load",function (event) {
    fillBoard();//LLena el tablero con la altura*largo de cuadros
    let game = new Game(height,width);//Crea un nuevo juego
    game.setup();//Prepara la entrada 
    game.start_game();//Inicia el juego
});


/**
 * FunciÃ³n que llena el tablero, con la cuadrÃ­cula
 */
function fillBoard(){
    //Llamo al tablero que es una tabla con el id board
    let board = document.getElementById("board");
    //Hago 2 for, este es para recorrer todas las filas
    for (let row = 0; row < width; row++) {
        var tr = document.createElement("tr");//Creo un elemento de tipo tr
        tr.id = "row_"+row;//Le asigno el id a esa fila
        //Este for sirve para recorrer todos los elementos de esa fila
        for (let index = 0; index < height; index++) {
            //Creo un elemento de la fila
            var td = document.createElement("td");
            td.className += " " + "box";//Les agrego la clase de Css box que tiene su estilo
            td.innerText = "";
            td.id = "row:"+row+"_column:"+index;//A este id estarÃ© haciendo referencia
            td.setAttribute("state","board");//Le pongo el estado, de esa manera coloreo del fondo
            tr.appendChild(td);//Se lo agrego como hijo a la fila
        }
        //Agrego esa fila y repito el ciclo con otra fila
        board.appendChild(tr);
    }
}
