import MoonLander from "./MoonLander.js";

window.addEventListener("load", function () {
    let canvas = document.getElementById("dinamic-canvas");
    let background = document.getElementById("static-canvas");
    let menu = document.getElementById("menu");
    let mensaje = document.getElementById("mensaje");
    let boton = document.getElementById("boton");
    let botones = document.getElementById("botones");

    let up = document.getElementById("up");//Botones
    let right = document.getElementById("right");
    let left = document.getElementById("left");

    /**Definición de tamaños de estilo */
    canvas.width = background.width =  360;//pongo el tamaño a los canvas
    canvas.height = background.height = 640;//Lo pongo aquí por si se quiere cambiar solo se cambia una linea
    menu.style.width = JSON.stringify(canvas.width)+"px";
    botones.style.width = JSON.stringify(canvas.width)+"px";
    botones.style.marginTop = JSON.stringify(canvas.height)+"px";
    /**Fin de definición de tamaños de estilo */

    //Empaqueto un bundle de cosas, para pasarlo a la función en gameOver
    let menu_set = {  boton: boton, menu: menu, mensaje: mensaje };

    //Creo un objeto moonlander, es la clase que empaqueta al juego
    let moonLander = new MoonLander(canvas, background);
    //Le pongo los botones de entrada
    moonLander.set_up_input(up,right,left);
    //asigno la función de GameOver, se ejecutará cuando el juegue acabe
    moonLander.onGameOver = (mensaje => {
        this.boton.innerText = "Volver a Jugar";
        this.menu.style.display = "block";
        this.mensaje.innerText = mensaje;
    }).bind(menu_set);
    //Le asigno la función al botón, cada vez que es presionado, inicia un juego nuevo,
    //Y esconde el menú
    boton.onclick = (function () { this[0].new_game();this[1].style.display = "none";}).bind([moonLander,menu]);
});

