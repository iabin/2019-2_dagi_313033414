import SpaceShip from "./SpaceShip.js";
const error_collition_range = 5;//Es el error en el cual se calculan las colisiones
/**
 * Clase que modela el juego MoonLander
 * Los métodos van en orden,
 * set_up_input()
 * new_game() inicia el nuevo juego
 * 
 * run() es el main loop del juego
 */
export default class MoonLander {
    constructor(canvas, background) {
        this.background = background;//Fondo
        this.canvas = canvas;//canvas dinámico
        this.onGameOver = (mensaje) => { console.log(mensaje) };//Función defauld onGameOver
        this.explotion = new Image();//CArgo la imagen de la explosión
        this.pressed_buttons = {};
        this.pressed_keys = {};
        this.explotion.onload = () => { console.log("Cargado") };
        this.explotion.src = 'https://cdna.artstation.com/p/assets/images/images/004/031/042/original/klemens-wohrer-beacon0005.gif?1479663362';

    }

    /**
     * Asigna los listeners de los eventos para la entrada
     * para las flechas y 3 elementos html cualesquiera
     * @param {HTMLElement} up_button 
     * @param {HTMLElement} right_button 
     * @param {HTMLElement} left_button 
     */
    set_up_input(up_button, right_button, left_button) {
        /**Cuando los presionas, pones el estado del botón en true */
        up_button.onmousedown = (function () { this.pressed_keys.up = true;  }).bind(this);
        right_button.onmousedown = (function () { this.pressed_keys.right = true;  }).bind(this);
        left_button.onmousedown = (function () { this.pressed_keys.left = true; }).bind(this);

        //Cuando liberas el mouse se pone el estado como undefined
        document.onmouseup = (function () { this.pressed_keys = {}; }).bind(this);
        //Esto lo hice porque cuando presionabas el botón, on click solo se llama una vez
        //Entonces si dejabas el botón presionado no pasaba más que una vez

        //Lo mismo pero con las flechas
        document.onkeydown = checkKey.bind(this);
        document.onkeyup = (function(){ this.pressed_keys = {};}).bind(this);
        
        function checkKey(e) {
            e = e || window.event;
         
           if (e.keyCode == '40') {
                this.pressed_keys.up = true;
            }
            else if (e.keyCode == '37') {
                this.pressed_keys.left = true;
            }
            else if (e.keyCode == '39') {
                this.pressed_keys.right = true;
            }
        }
    }

    /**
     * Reinicializa las variables, re carga la imagen de la nave, y 
     * ejecuta la función run() que es el mainLoop
     */
    new_game() {
        this.pressed_keys = {};
        this.terrain = {};
        this.playing = true;
        this.generate_terrain();//Genera un nuevo terreno aleatorio
        this.ship = new SpaceShip(this.canvas.width);
        this.lastTime = new Date();
        this.img = new Image();
        this.img.onload = function () {//Hago esto para que se llame el main loop después de cargar la imagen
            console.log("nave cargada ");
            this.run();
        }.bind(this);
        this.img.src = 'https://joellongi.bitbucket.io/2019-2/practicas/imgs/p06/space_ship.svg';
        //drawing of the test image - img1
    }

    /**
     * Main loop del juego
     * 1.- Limpia el canvas dinámico 
     * 2.- Revisa el estado de la entrada y modifica a la nave según este
     * 3.- actualiza al siguiente estado de la nave
     * 4.- Lo pinta en el canvas ya limpio
     */
    run() {
        this.clean(this.canvas);
        this.process_input();
        this.update();
        this.render();
        if (this.playing)//Si la condición de jugar está puesta, continua
            window.requestAnimationFrame(this.run.bind(this));
    }

    /**
     * Actualiza el estado interno de la nave
     */
    update() {
        //Revisa si ya perdí, si ya perdí, la siguiente iteración ya no se realizará
        this.check_lose();
        var currDate = new Date();
        var elapsed = (currDate - this.lastTime) / 1000;//Elapsed, es el tiempo que pasó entre iteraciones
        this.ship.next_position(elapsed);//Le paso elapsed para que lo use en los cálculos
        this.lastTime = currDate;
    }
    /**
     * Revisa si ya perdí
     * si ya perdí, modifico la bandera this.playing
     */
    check_lose() {
        let y = this.terrain[Math.trunc(this.ship.x)];//Obtengo la altura en y del punto x
        //Hago trunc porque la coordenada de la nave está en flotantes, y el terreno son enteros
        if (y) {//Si y está definida, es porque está en los puntos de la montaña
            if (this.ship.y + error_collition_range > y) {//Reviso si ya choqué
                this.onGameOver("Perdiste");//si es así mando la función onGameOver
                drawImage(this.canvas, this.explotion, this.ship.x - this.explotion.width / 10, this.ship.y - this.explotion.height / 5, this.explotion.width / 5, this.explotion.height / 5);
                //Dibujo la explosión
                this.playing = false;
            }
            //Si y no está, significa que es un punto a salvo
        } else if (this.ship.y > this.safeplace.y) {
            if (this.ship.speed.y > 50) {//Si voy muy rápido, pierdo
                this.onGameOver("too fast :C");
                this.playing = false;
            } else {//Si voy a menos de 50 en velocidad vertical, entonces gano
                this.onGameOver("Ganaste");
                this.playing = false;
            }
        }
    }

    /**Proceso la entrada */
    process_input() {
        if (this.pressed_keys.up)
            this.ship.speed.y += -10;//Aumenta mi velocidad en 10 vertical
        if (this.pressed_keys.left)
            this.ship.speed.x += 5;//Lo mismo
        if (this.pressed_keys.right)
            this.ship.speed.x += -5;
       
    }

    /**
     * Solo dibuja el png de la imagen en las coordenadas dadas, y lo reescala
     */
    render() {  
        drawImage(this.canvas, this.img, this.ship.x - this.img.width / 10, this.ship.y - this.img.height / 5, this.img.width / 5, this.img.height / 5);
    }

    /**
     * La función para generar el terreno
     * Limpia el canvas del fondo
     * Calcula una meseta de aterrizaje, de posisión y longitud random
     * Luego calcula las montañas, desde el fin de la plataforma al fin del canvas
     * y calcula otras montañas, desde el principio de la plataforma al principio del canvas
     */
    generate_terrain() {
        this.clean(this.background);
        let safeplace_x = Math.random() * ((this.canvas.width - 40) + 20) - 20;
        let safeplace_y = Math.random() * (this.canvas.height - this.canvas.height / 1.5) + this.canvas.height / 1.5;
        let safeplace_length = (Math.random() * (100 - 50) + 50);
        this.safeplace = { x: Math.trunc(safeplace_x), y: Math.trunc(safeplace_y), length: Math.trunc(safeplace_length) };//Los pasos pasados, fueron para
        //generar un vector aleatorio y una longitud que será la base
        this.calculate_and_draw_mountains();//Ya generé el safeplace, ahora a partir de él genero las montañas
    }


    calculate_and_draw_mountains() {
        var STEP_MAX = 2.9;//Pendiente máxima, si lo aumentas las montañas serán más pronunciadas
        //si lo disminuyes todo será más liso

        var STEP_CHANGE = 1; //Probabilidad de un cambio, cantidad máxima que se suma o se le resta
        // a la pendiente en cada iteración
        //si lo aumentas habrá muchos picos pequeños, porque muy seguido se cambia la pendiente
        //Si lo disminuyes, todo será super liso y continuo

        var HEIGHT_MAX = this.background.height;//Es importante resaltar que este es el suelo
        var HEIGHT_MIN = this.background.height / 2;//La altura máxima que puede alcanzar una montaña es la mitad

        // Condiciones iniciales
        var height = this.safeplace.y;//Voy a empezar desde la altura del punto a salvo, para 
        //que parezca que el punto a salvo es parte del terreno
        var slope = (Math.random() * STEP_MAX) * 2 - STEP_MAX;//Hago esto para que me dé un número entre
        // menos la pendiente máxima a la pendiente máxima


        //Función que nos da el punto y actual y la nueva pendiente
        //Usando la pendiente del estado pasado y dibuja una linea de esa y al suelo
        var calculate = (x) => { //Esta función solo la hago para reutilizar código abajo
            height += slope;//mi altura es la altura anterior, más la pediente

            //Aquí sucede la mágia del algoritmo, como los número de random tienen una proporción
            //más o menos uniforme, la cantidad de números simétricos respecto al eje 0 serán parecidos,
            //por ejemplo 1 y -1 
            //De esta manera, la pendiente no suele variar mucho y siempre tiende a conservar su valor
            //o un valor parecido
            //Pero cuando eventualmente, algún número muy grande aparece, la pendiente tiene un cambio brusco
            //digamos, que cada pico o depresión de la montaña, es visto como un número grande que cambió 
            //brúscamente el valor de la pendiente, y por la distribución de los números
            //Suele mantenerse en un valor parecido la mayoría del tiempo
            slope += (Math.random() * STEP_CHANGE) * 2 - STEP_CHANGE;

            // Aquí estoy acotando el valor máximo de la pendiente
            if (slope > STEP_MAX) { slope = STEP_MAX };
            if (slope < -STEP_MAX) { slope = -STEP_MAX };

            //Si pasa de los valores máximos, entonces invertimos la pendiente
            //Estos son picos artificiales, que hacemos para que no se salgan
            //las montañas o depresiones del rango dado
            if (height > HEIGHT_MAX) {
                height = HEIGHT_MAX;
                slope *= -1;
            }

            if (height < HEIGHT_MIN) {
                height = HEIGHT_MIN;
                slope *= -1;
            }
            this.terrain[x] = height;
            //Dibujamos una linea de donde estamos, al punto de abajo del canvas
            //Tiene grosor 10, para que se vea negro, si lo hago de grosor 1 se verá gris, por 
            //la forma rara en la que pinta el canvas
            draw_line(this.background, x, HEIGHT_MAX, x, height, "gray", 10);
        };


        // creación del terreno, desde el fin del punto a salvo, para que parezca que pertenece al terreno
        for (var x = this.safeplace.x + this.safeplace.length; x < this.background.width; x++) {
            calculate(x);
        }

        //Hago exactamente lo mismo, pero ahora voy, desde el inicio del  punto a salvo al 0
        //En el for pasado lo hice del fin del punto a salvo al fin del canvas
        height = this.safeplace.y;//Empiezo desde la altura del punto a salvo para que se vea natural
        slope = (Math.random() * STEP_MAX) * 2 - STEP_MAX;//Obtengo una pendiente random nueva
        for (var x = this.safeplace.x; x > 0; x--) {//Voy disminuyendo hasta llegar a 0
            calculate(x);
        }
        //Dibuja el rectangulo negro que es la base
        draw_rect(this.background, this.safeplace.x, this.safeplace.y, this.safeplace.length, this.background.height - this.safeplace.y, "gray");
        //Dibuja una linea amarilla al rededor, para hacer saber al jugador donde bajar
        draw_line(this.background, this.safeplace.x, this.safeplace.y + 3, this.safeplace.x + this.safeplace.length, this.safeplace.y + 3, "yellow", 5);
    }

    /**función que limpia un canvas dado */
    clean(canvas) {
        let context = canvas.getContext('2d');
        context.clearRect(0, 0, this.canvas.width, this.canvas.height);
    }
}
