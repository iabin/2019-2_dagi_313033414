
function draw_line(canvas, x1, y1, x2, y2, color = 'black', width = 1) {
    var ctx = canvas.getContext("2d");
    ctx.strokeStyle = color;
    ctx.lineWidth = width;
    ctx.beginPath();
    ctx.moveTo(x1, y1);
    ctx.lineTo(x2, y2);
    ctx.stroke();
}

function drawImage(canvas,image,x,y,width,height){
    var ctx = canvas.getContext('2d');
    ctx.drawImage(image,x,y,width,height);
}

function draw_rect(canvas, x, y, width, height,color='black', mode = "fill") {
    var ctx = canvas.getContext("2d");
    ctx.rect(x, y, width, height);
    ctx.fillStyle = color; // Red color
    if (mode === 'stroke')
        ctx.stroke();
    if (mode === 'fill')
        ctx.fill();
}

function draw_circle(canvas, x, y, r, color = 'black') {
    var ctx = canvas.getContext("2d");
    ctx.fillStyle = color; // Red color
    ctx.beginPath(); //Start path
    ctx.arc(x, y, r, 0, Math.PI * 2, true); // Draw a point using the arc function of the canvas with a point structure.
    ctx.fill(); // Close the path and fill.
}

function write_text(canvas, text, x, y,color="black") {
    let ctx = canvas.getContext('2d');
    ctx.font = "30px Arial";
    ctx.textBaseline = 'bottom';
    ctx.fillStyle = color;
    ctx.fillText(text, x, y);
}