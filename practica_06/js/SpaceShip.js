export default class SpaceShip {
    constructor(limit_x) {
        this.limit_x = limit_x;
        this.x = 50;
        this.y = 50;
        this.speed = { x: 10, y: 70 };
        this.gravity = 30;
        this.FRICTION = 0.999;
    }

    check_limits() {
        if (this.x > this.limit_x)
            this.x = 0;
        if (this.x < 0)
            this.x = this.limit_x;
    }

    next_position(elapsed) {
        this.speed.y = this.speed.y + this.gravity * elapsed;
        this.speed.x *= this.FRICTION;
        this.speed.y *= this.FRICTION;
        this.x = this.x + this.speed.x * elapsed;
        this.y = this.y + this.speed.y * elapsed;
        this.check_limits();
    }
}