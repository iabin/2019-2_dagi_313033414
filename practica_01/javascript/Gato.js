export default class Gato{
    //Funcion que construye el tablero y agrega la función on click a cada casilla
    constructor(){
        this.tiros = 0;
        this.casillas = document.getElementsByClassName("casilla");//Todos los elementos con la clase
        //casilla son casillas así que los guardo
        this.jugador = 1;//Empieza siempre el jugador 1
        let empty = {
            1: 0,
            2: 0,
            3:0,
            4:0,
            5:0,
            6:0,
            7:0,
            8:0,
            9:0,
        };
        this.game = document.getElementById("game");
        this.dict = empty;//Asigno el tablero
        this.turno = document.getElementById("turno");
        this.imagenTurno = document.getElementById("imagen_turno");
        this.avisos =  document.getElementById("avisos");
        this.startMenu = document.getElementById("startMenu");
        this.turno.textContent = "Es turno del jugador  1";
        this.boton = document.getElementById("boton");

        //Para cada elemento del tablero le agrego esta función
        for (var index = 0; index<this.casillas.length;index++ ){
            let a  = this.casillas[index];
            a.addEventListener("mousedown",()=> {
                let b  = a;//Esta variable me dió un dolor de cabeza, ya que siempre que se llamaba
                //guardaba el último valor puesto, así guardo una copia local en cada función
                let image =  b.children[0];//La imagen hijo a mi div, lo usé en vez de background
                let casillaPropia = this.dict[b.id];//La casilla donde se llama la función
                if (casillaPropia === 0){//Si es 0 nunca se ha tirado y se puede tirar
                    if (this.jugador === 1)
                    {
                        this.tiros++;
                        image.src="media/cross.svg";//Ia imagen ahora es la cruz
                        image.style.visibility='visible';// y la pongo visible
                        this.dict[b.id] = 1;//Marco la casilla como tirada por el jugador 1
                        if (this.isOver()){//Me pregunto si maté el juego
                            this.avisos.textContent = "Jugador 1 ganó";
                            this.startMenu.style.visibility = "visible";
                            this.game.style.pointerEvents =  "none";
                            this.boton.addEventListener("mousedown",()=> {this.gameOver()})

                        }
                        else if(this.tiros === 9){
                            this.avisos.textContent = "Empate";
                            this.startMenu.style.visibility = "visible";
                            this.game.style.pointerEvents =  "none";
                            this.boton.addEventListener("mousedown",()=> {this.gameOver()})
                        }
                        this.jugador = 2;//Ahora es turno del otro jugador
                        this.turno.textContent = "Es turno del jugador  2";
                        this.imagenTurno.src = "media/circle.svg"


                    }else if (this.jugador === 2)//Si es turno del jugador 2
                    {
                        this.tiros++;
                        image.src="media/circle.svg";//Ahora pongo la imagen del círculo
                        image.style.visibility='visible';//Lo pongo visible
                        this.dict[b.id] = 2;//Marco la casilla tirada por mi
                        if (this.isOver()){//Pregunto si maté el juego
                            this.avisos.textContent = "Jugador 1 ganó";
                            this.startMenu.style.visibility = "visible";
                            this.game.style.pointerEvents =  "none";
                            this.boton.addEventListener("mousedown",()=> {this.gameOver()})

                        }
                        else if(this.tiros === 9){
                            this.avisos.textContent = "Empate";
                            this.startMenu.style.visibility = "visible";
                            this.game.style.pointerEvents =  "none";
                            this.boton.addEventListener("mousedown",()=> {this.gameOver()})
                        }
                        this.jugador = 1;//Le toca al jugador 1
                        this.turno.textContent = "Es turno del jugador  1";
                        this.imagenTurno.src = "media/cross.svg"

                    }
                }
            });
        }
    }

    //Me dice si hay 3 iguales en las filas
    isOverFila(i){
        if(this.dict[i]===0){
            return false;
        }
        return (this.dict[i] === this.dict[i + 1]) && (this.dict[i + 2] === this.dict[i + 1])
    }
    //Me dice si hay 3 iguales en columnas
    isOverColumn(i){
        if(this.dict[i]===0){
            return false;
        }
        return (this.dict[i] === this.dict[i + 3]) && (this.dict[i + 3] === this.dict[i + 6]);
    }
    ifOverCross(){//Me dice si hay 3 iguales en las cruces

        var diag1 = (this.dict[1]===this.dict[5])&&(this.dict[5]===this.dict[9])&&(this.dict[1]!==0);
        var diag2 = (this.dict[3]===this.dict[5])&&(this.dict[5]===this.dict[7])&&(this.dict[3]!==0);
        return diag1||diag2;
    }
    //Funcion que devuelve true si alguien ganó el juego
    isOver() {

        var fila = this.isOverFila(1)||this.isOverFila(4)||this.isOverFila(7);
        var columna = this.isOverColumn(1)||this.isOverColumn(2)||this.isOverColumn(3);

        console.log(fila||columna||this.ifOverCross());
        return (fila||columna||this.ifOverCross());
    }

    //Función que se llama al acabar al juego
    gameOver(){
        location.reload();
    }


}