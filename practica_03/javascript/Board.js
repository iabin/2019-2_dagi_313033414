const square_size = 12;//Tamaño en pixeles del cuadro
const board_color = "#40c9a2";//COlor del tablero
const apple_color = "#b6244f";//Color de la manzana
const snake_color = "#212922";//COlor de la serpiente
const stroke_color = "#efeff0";//Color de la linea al rededor de los cuadro
export default class Board {//dont step on snek
    constructor(width, height) {
        this.canvas = document.getElementById("canvas");
        this.width = width;//Tamaño en CUADROS del tablero
        this.height = height;
        this.board = {};//Diccionario que contiene el estado del tablero
        this.toUpdate = [];//lista que contiene los nodos que se hayan modificado
        //ESto lo hice para que no se modificara todo, cada vez que se actualiza

        for (let row = 0; row < height; row++) {//Rellenamos el diccionario con todo vacio
            for (let column = 0; column < width; column++) {
                this.board["row:" + row + "_column:" + column] = 'board';
            }
        }
    }
    /**
     * Genera una nueva manzana en el tablero
     */
    new_apple() {
        let x = Math.floor(Math.random() * this.height);
        let y = Math.floor(Math.random() * this.width);
        //obtengo la casilla random 
        let potencial_casilla = this.board["row:" + y + "_column:" + x];
        var vector = [];
        vector.x = x;
        vector.y = y;
        //Si tiene estado tablero, pongo la manzana ahí
        if (potencial_casilla === "board") {
            this.board["row:" + y + "_column:" + x] = 'apple';
            this.toUpdate.push([vector, 'apple']);
            return;
        }
        //Si no es tablero, es serpiernte y vuelvo a llamar a la función
        return this.new_apple();
    }

    /**
     * Dibuja todos los elementos que hayan cambiado y estén en la lista de 
     * toUpdate
     */
    render_board() {
        var ctx = this.canvas.getContext("2d");//Obtener el contexto2d
        this.toUpdate.forEach(element => {//Iterar sobre los elementos que hayan cambiado
            let estado = element[1];//el segundo elemento es el nuevo estado
            let vector = element[0];//El primer elemento es la posición 
            if ((estado == "board")) {
                ctx.fillStyle = board_color;
            } else if ((estado == "snake")) {
                ctx.fillStyle = snake_color;
            } else if ((estado == "apple")) {
                ctx.fillStyle = apple_color;
            }
            //Dibujo los nuevos cuadros y sus bordes
            ctx.fillRect(vector.x * square_size, vector.y * square_size, square_size, square_size);
            ctx.strokeRect(vector.x * square_size, vector.y * square_size, square_size, square_size);

        });
        //Dibujar
        ctx.stroke();
        this.toUpdate = [];//Reiniciar la lista de pendientes por dibujar
    }

    /**
     * Dibuja la tabla inicialmente
     * solo se ejecuta al cargar la ventana
     */
    draw_table() {
        let pixel_width = square_size * this.width;//Obtengo la cantidad de pixeles, 
        //Multiplico al ancho por el grosor de cada cuadro
        let pixel_height = square_size * this.height;
        this.canvas.width = pixel_width;//Se lo pongo al elemento html
        this.canvas.height = pixel_height;
        var ctx = this.canvas.getContext("2d");
        ctx.beginPath();
        ctx.lineWidth = 1;//El grosor de linea
        ctx.fillStyle = board_color;//el color del tablero
        ctx.strokeStyle = stroke_color;
        var svgns = 'http://www.w3.org/2000/svg';

        //Este doble for, llena todos y cada uno de los cuadros del tablero
        for (let row = 0; row < this.height; row++) {
            for (let column = 0; column < this.width; column++) {


                ctx.fillRect(column * square_size, row * square_size, square_size, square_size);
                ctx.strokeRect(column * square_size, row * square_size, square_size, square_size);
            }
        }
        //Dibuja todo al final
        ctx.stroke();
    }
}