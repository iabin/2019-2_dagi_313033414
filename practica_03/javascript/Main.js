
import Game from './Game.js';

const width = 64;//Largo
const height = 48;//Alto

//Al terminar de cargar se ejecuta esto
window.addEventListener("load",function (event) {
    let game = new Game(height,width);//Crea un nuevo juego
    game.setup();//Prepara la entrada 
    game.start_game();//Inicia el juego
});

