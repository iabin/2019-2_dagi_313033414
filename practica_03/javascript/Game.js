import Snek from './Snek.js'
import Board from './Board.js'
const MS_PER_FRAME = 100;

export default class Game {//Main thread
    constructor(height, width) {
        this.board = new Board(width, height);
        this.scoreboard = document.getElementById("score");//El score
        this.snake = new Snek(3, 5);//Nueva serpiente con esa posición
        this.score = 0;//Empieza en 0
        this.height = height;
        this.width = width;
        this.hud = document.getElementById("HUD");//El hud de gameOVer
        this.retry_button = document.getElementById("retry");//El botón de jugar otra vez
        this.finished = false;//Revisa si el juego está acabado
        this.speed = this.snake.speed;//La velocidad de la serpiente, 
        //Se actualiza aquí y en cada ciclo se le pasa a la serpiente

    }

    //Prepara la entrada
    setup() {
        this.input();
        this.board.draw_table();
    }

    //La función main del juego
    async start_game() {
        this.board.new_apple();    
        setInterval(() => {//Ciclo que se ejecuta cada segundo Como uso this.snake uso la lambda
            if (this.finished) {
                return;
            }
            this.update();
            this.board.render_board();
            this.render_score();
        }, MS_PER_FRAME);
    }

    render_score() {
        //Es llamado cada que come manzana, solo actualiza el score
        this.scoreboard.innerHTML = "Score: " + this.score;
    }

    move_and_update_eating() {
        this.score += 1;
        let next_move = this.snake.potencial_next_move();//Reviso el potencial 
        this.snake.next_move_eating();
        this.snake.body.forEach(element => {
            this.board.toUpdate.push([next_move,'snake']);
            this.board.board["row:" + element.y + "_column:" + element.x] = 'snake';
        });
        this.board.new_apple();
    }

    move_and_update_no_eating() {
        let next_move = this.snake.potencial_next_move();//Reviso el potencial 
        let old = this.snake.next_move_no_eating();
        this.snake.body.forEach(element => {
            this.board.toUpdate.push([next_move,'snake']);
            this.board.board["row:" + element.y + "_column:" + element.x] = 'snake';
        });
        this.board.toUpdate.push([old,'board']);
        this.board.board["row:" + old.y + "_column:" + old.x] = 'board';
    }

    update() {//Traté de separarlos pero, para saber el estado del tablero necesito acceso al document
        this.snake.setSpeed(this.speed.x, this.speed.y);//Le pongo la dirección dada por la entrada 
        let potencial_move = this.snake.potencial_next_move();//Reviso el potencial 
        //Siguiente movimiento para revisar si perdí y obtener la casillas
        let potencial_casilla = this.board.board["row:" + potencial_move.y + "_column:" + potencial_move.x];
        //Si es null significa que no existe esa casilla, eso significa que no existe y se terminó el tablero
        if (potencial_casilla == null) {
            this.gameOver();//LLamos el gameOver
            return;
        }
        //Si es una manzana, Llamo al método que come aunmenta el tamaño y actualiza el board
        if (potencial_casilla == "apple") {
            this.move_and_update_eating();
            //Solo me muevo y renderizo
        } else if ((potencial_casilla == "board")) {
            this.move_and_update_no_eating();
            //LLamo al game over
        } else if ((potencial_casilla == "snake")) {
            this.gameOver();
            return;
        }
    }



    gameOver() {
        //El hud de game over lo pongo visible
        this.hud.style.visibility = "visible";
        //La bandera de finished es true
        this.finished = true;
    }

    //Se carga al principio y carga los listeners
    input() {
        //Es el boton de game over, recarga la página
        this.retry_button.addEventListener("click", () => {
            location.reload();
        });
        //Carga los eventos del teclado
        addEventListener("keydown", (event) => {
            //Cada click modifica la dirección en el juego, y se actualiza en la serpiente
            //En la función update an render
            if (event.keyCode == 37) {//Left arrow
                this.speed = this.snake.newVector(-1, 0);
            } else if (event.keyCode == 38) {//Up arrow
                this.speed = this.snake.newVector(0, -1);
            } else if (event.keyCode == 39) { //Right arrow
                this.speed = this.snake.newVector(1, 0);
            } else if (event.keyCode == 40) {//Down arrow
                this.speed = this.snake.newVector(0, 1);
            }

        }, false);
    }




}

